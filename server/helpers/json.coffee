module.exports =

  replacer: (key, value) ->
    # we don't want the client to see __v in output json

    if key == '__v'
      undefined
    else
      value
