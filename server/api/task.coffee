express     = require 'express'
createError = require('http-errors');

Task        = require '../models/task'


router = express.Router()


# Standard REST methods GET, PUT, POST and DELETE ------------------------------
router.get '/', (req, res, next) ->
  Task.find {}
  .then (data) ->
    res.json data
  .catch next


router.post '/', (req, res, next) ->
  task = new Task text: req.body.text

  task.save()
  .then (doc) ->
    res.json success: 'Task Created', task: doc
  .catch next


router.delete '/:id', (req, res, next) ->
  docId = req.params.id

  Task.findByIdAndRemove _id: docId
  .then (doc) ->
    throw createError 404, 'Task Not Found' if !doc
    res.json success: 'Task deleted'
  .catch next


router.get '/:id', (req, res, next) ->
  docId = req.params.id

  Task.findById docId
  .then (doc) ->
    throw createError 404, 'Task Not Found' if !doc
    res.json doc
  .catch next


router.put '/:id', (req, res, next) ->
  docId = req.params.id

  updateQuery = $set: {}
  for prop, value of req.body
    updateQuery.$set[prop] = value

  Task.findByIdAndUpdate docId, updateQuery
  .then (doc) ->
    throw createError 404, 'Task Not Found' if !doc
    res.json success: 'Task updated'
  .catch next


# Error middleware. Catch exceptions here and send back correct json
router.use (err, req, res, next) ->

  if err instanceof createError.NotFound
    res.status(err.statusCode).json error: err.message
  else
    res.status(500).json error: 'Internal Error'

module.exports = router
