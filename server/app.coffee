express    = require 'express'
bodyParser = require 'body-parser'


# Intantiate the main express instance
app = express()

# Configure express from config file
expressConfig = (require './config/server.config').expressConfig
for property, value of expressConfig
  app.set property, value

# Register express middleware
app.use bodyParser.json()
app.use bodyParser.urlencoded extended: true

# Register routes
homeRoutes = require './routes/home'
app.use '/', homeRoutes

# Register api routes
taskApi = require './api/task'
app.use '/api/task', taskApi


# As we directly jsonify mongoose objects and return, remove the metadata
# vars such as __v.
jsonHelper = require './helpers/json'
app.set 'json replacer', jsonHelper.replacer


module.exports = app
