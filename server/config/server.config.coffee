path      = require 'path'

# Make Bluebird the promise provider for mongoose
mongoose  = require 'mongoose'
mongoose.Promise = require 'bluebird'

# Express configurations
expressConfig =
  'views':           path.resolve __dirname, '../', 'views'
  'view engine':     'pug'

# Mongoose configurations
mongoConfig =
  dev:
    url: 'mongodb://localhost/devdb'
  test:
    url: 'mongodb://localhost/testdb'

module.exports =
  expressConfig: expressConfig
  mongoConfig: mongoConfig
