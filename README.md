# Express+Mongoose REST api in CoffeeScript starter project #

A starter project for writing REST api with ExpressJS and MongooseJS, witten
in CoffeeScript.


## Features ##

 1. Vagrant

 1. node 6

 I use Node 6.x in this vagrant. But there is nothing in this code that stops
 it from being run on node 4.x and even 0.10.x/0.12.x.

 1. List of Frameworks/Libs
  * ExpressJS
  * MongooseJS
  * mocha
  * chai
  * gulp
