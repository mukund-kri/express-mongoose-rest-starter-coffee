
gulp      = require 'gulp'
server    = require 'gulp-develop-server'
mocha     = require 'gulp-spawn-mocha'
runSeq    = require 'run-sequence'
coffee    = require 'gulp-coffee'
gutil     = require 'gulp-util'
clean     = require 'gulp-clean'


SERVER_CODE = './server/**/*.coffee'
VIEWS_DIR = './server/views'
PRODUCTION_BUILD_DIR = './build'


mochaOpts =
  compilers: [
    'coffee:coffee-script/register'
  ]

# --------------------------- DEV TASKS ----------------------------------------
# Use gulp-develop-server to run the server in development mode
gulp.task 'server:start', () ->
  server.listen { path: './server/develop.coffee' }

# Restart dev server on code change
gulp.task 'watch:server', () ->
  gulp.watch SERVER_CODE, server.restart

# Aggrigate all development tasks under one name
gulp.task 'develop', ['server:start']


# The default is just an alias for develop
gulp.task 'default', ['develop']


# ------------------------- TESTING TASKS --------------------------------------
gulp.task 'test:models', () ->
  gulp.src ['tests/server/models/*.spec.coffee']
  .pipe (mocha mochaOpts)

gulp.task 'test:routes', () ->
  gulp.src ['tests/server/routes/*.spec.coffee']
  .pipe (mocha mochaOpts)

gulp.task 'test:api', () ->
  gulp.src ['tests/server/api/*.spec.coffee']
  .pipe (mocha mochaOpts)

# Run all tests
gulp.task 'test', () ->
  runSeq 'test:models', 'test:routes', 'test:api'

# -------------------------- PRODUCTION TASKS --------------------------------
# Compile all coffeescript files into build directory
gulp.task 'build:coffee', ->
  gulp.src SERVER_CODE
  .pipe (coffee {bare: true}).on 'error', gutil.log
  .pipe (gulp.dest PRODUCTION_BUILD_DIR)

# Copy the express views folder
gulp.task 'build:views', ->
  gulp.src "#{VIEWS_DIR}/**/*"
  .pipe gulp.dest "#{PRODUCTION_BUILD_DIR}/views"

# nuke the build folder
gulp.task 'clean', ->
  gulp.src PRODUCTION_BUILD_DIR, {read: false}
  .pipe clean()

# bring together all production build tasks
gulp.task 'build', ['build:coffee', 'build:views']

  
