expect      = (require 'chai').expect
mongoose    = require 'mongoose'

Task        = require '../../../server/models/task'
fakerHelper = require './faker.helper'
dbUrl       = (require '../../../server/config/server.config').mongoConfig.test.url
clearDB     = (require 'mocha-mongoose')(dbUrl, {noClear: true})


describe 'Task Model', ->

  # Check if mongoose is connected to mongo, if not connect
  before (done) ->
    return done() if mongoose.connection.db
    mongoose.connect dbUrl, done

  # Clean up after all tests are done
  after (done) ->
    clearDB done


  it '#Can instaiate an instance, instance can be saved', (done) ->
    task = new Task {text: 'Task 1'}
    task.save()
    .then (doc) ->
      expect(doc.__v).to.not.be.undefined
      expect(doc._id).to.be.ok
      expect(doc).property('status', false)
      return
    .then done

  it '#Can be created', (done) ->
    fakeTaskData = fakerHelper.genTaskData()

    Task.create fakeTaskData
    .then (doc) ->
      expect(doc.__v).not.to.be.undefined
      expect(doc).property('text', fakeTaskData.text)
      return
    .then done

  it '#Records can be listed', (done) ->
    Task.find {}
    .then (docs) ->
      expect(docs.length).to.equal(2)
      return
    .then(done)

  # TODO: You know the drill, add your tests here ...
