expect         = (require 'chai').expect
mongoose       = require 'mongoose'
supertest      = require 'supertest'
fixtures       = require 'node-mongoose-fixtures'

app            = require '../../../server/app'
config         = require '../../../server/config/server.config'
Task           = require '../../../server/models/task'

clearDB        = (require 'mocha-mongoose')(config.mongoConfig.test.url, {noClear: true})


describe 'Task API', ->

  before (done) ->
    return done() if mongoose.connection.db
    mongoose.connect config.mongoConfig.test.url, done

  before (done) ->
    fixtures (require '../fixtures/many.tasks'), done

  after (done) -> clearDB done


  it '#GET should return all tasks', (done) ->
    supertest app
    .get '/api/task'
    .expect 'content-type', /json/
    .expect 200
    .end (err, res) ->
      throw err if err

      expect(res.body.length).to.equal 2
      done()

  it '#POST should create a new record', (done) ->
    supertest app
    .post '/api/task'
    .send text: 'posted task'
    .expect 'content-type', /json/
    .expect 200
    .end (err, res) ->
      throw err if err

      expect(res.body).property 'success', 'Task Created'
      expect(res.body).deep.property 'task.text', 'posted task'

      # Look into the database if a task record is Created
      Task.find text: 'posted task'
      .then (doc) ->
        expect(doc[0]._id).to.be.ok
        return
      .then done

  it '#DELETE should remove a record', (done) ->
    supertest app
    .delete '/api/task/500000000000000000000001'
    .expect 200
    .expect 'content-type', /json/
    .end (err, res) ->
      throw err if err
      expect(res.body).property 'success', 'Task deleted'

      # Test the db if the record is deleted
      Task.findById '500000000000000000000001'
      .then (doc) ->
        expect(doc).to.not.be.ok   # Record should not exist
        return
      .then done

  it '#DELETE on a non existant record should be 404', (done) ->
    supertest app
    .delete '/api/task/500000000000000000000004'
    .expect 404
    .expect 'content-type', /json/
    .expect error: 'Task Not Found'

    done()

  it '#GET with id should return a single task', (done) ->
    supertest app
    .get '/api/task/500000000000000000000002'
    .expect 200
    .expect 'content-type', /json/
    .end (err, res) ->
      throw err if err
      expect(res.body).property 'text', 'Task 2'

      done()

  it '#PUT should update a document', (done) ->
    supertest app
    .put '/api/task/500000000000000000000002'
    .send text: 'updated task'
    .expect 200
    .expect 'content-type', /json/
    .end (err, res) ->
      throw err if err
      expect(res.body).property 'success', 'Task updated'

      # Check the database if the property is updated
      Task.findById '500000000000000000000002'
      .then (doc) ->
        expect(doc).property 'text', 'updated task'
        return
      .then done
