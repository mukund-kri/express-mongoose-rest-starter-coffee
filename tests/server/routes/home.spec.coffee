expect     = (require 'chai').expect
supertest  = require 'supertest'

app        = require '../../../server/app'


describe 'Home page', ->

  it '#Home page should render correctly', (done) ->
    supertest app
    .get '/'
    .expect 'content-type', /text/
    .expect 200
    .end (err, res) ->
      throw err if err

      expect(res.text).to.match /Your\ App/
      done()
